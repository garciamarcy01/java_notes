package com.aplicacion.api;

import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration;
import org.springframework.boot.autoconfigure.data.mongo.MongoRepositoriesAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@SpringBootApplication(exclude = { MongoAutoConfiguration.class, MongoDataAutoConfiguration.class,
		MongoRepositoriesAutoConfiguration.class, SecurityAutoConfiguration.class })
@EnableAspectJAutoProxy
@ComponentScan("com.aplicacion")
public class App {
	
	private static final Logger LOG = LoggerFactory.getLogger(App.class);

	public static void main(String[] args) {
		System.setProperty("ambiente", "desarrollo");
		SpringApplication.run(App.class, args);
		
		
		try {
			Timer timer = new Timer(); 
			TimerTask tt = new TimerTask(){
				public void run(){
					Calendar cal = Calendar.getInstance(); //this is the method you should use, not the Date().
	 
					int hour = cal.get(Calendar.HOUR_OF_DAY);//get the hour number of the day, from 0 to 23
	 
					if(hour == 12){
						LOG.info("Realizar Tarea de refrescar Arcus Data en DB.");
						CatchingComponent.guardarArcusData();
					}
				}
			};
			
			timer.schedule(tt, 1000, 1000*60*60*24); // Ejecuta tarea cada 24 horas
		}catch (Exception e) {
			LOG.info("Incidencia... {}", e.getMessage());
		}
		

		
	}

}
